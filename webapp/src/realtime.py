"""
Real time service provide update streams for the web clients.
"""
import alerting
import outputs

socketio = None
NAMESPACE = '/stream'


def _emit(msg):
    """
    Broadcast a message to listening clients.
    """
    if socketio:
        socketio.emit('update', msg, namespace=NAMESPACE)


def dispatch_measure(measure_time, metric, value):

    measure = outputs.measure(measure_time, metric, value)
    msg = outputs.response([measure], [], alerting.get_statuses())
    _emit(msg)


def dispatch_start_alert(start_time, metric, value):

    alert = outputs.alert(start_time, metric, value, outputs.EVENT_START_ALERT)
    msg = outputs.response([], [alert], alerting.get_statuses())
    _emit(msg)


def dispatch_stop_alert(stop_time, metric, value):

    alert = outputs.alert(stop_time, metric, value, outputs.EVENT_STOP_ALERT)
    msg = outputs.response([], [alert], alerting.get_statuses())
    _emit(msg)
