// Utils
Object.filter = function(obj, predicate) {

    var res = {};

    for (var key in obj) {
        if (obj.hasOwnProperty(key) && predicate(key, obj[key])) {
            res[key] = obj[key];
        }
    }

    return res;
};

Object.values = function(obj) {

    return Object.keys(obj).map(function(key){
        return obj[key];
    });
};

// Constants
var DEFAULT_DATA_TO_DISPLAY = 10 * 60; // seconds
var EVENT_START_ALERT = "start_alert";
var EVENT_STOP_ALERT = "stop_alert";

var updateData = function(allData, newData) {

    allData.statuses = newData.statuses;

    // filter out duplicates using identical times
    var measures = allData.measures;
    for(var i = 0; i < newData.measures.length; i++) {
        var measure =  newData.measures[i];
        measures[measure.metric + measure.time] = measure;
    }
    var alerts = allData.alerts;
    for(var i = 0; i < newData.alerts.length; i++) {
        var alert =  newData.alerts[i];
        alerts[alert.metric + alert.time] = alert;
    }

    // filter outdated data
    var tenMinutesAgo = new Date().getTime() / 1000 - DEFAULT_DATA_TO_DISPLAY;
    measures = Object.filter(measures, function(k, v) { return v.time >= tenMinutesAgo; });
    alerts = Object.filter(alerts, function(k, v) { return v.time >= tenMinutesAgo; });

    allData.measures = measures;
    allData.alerts = alerts;
};

$(document).ready(function() {

    var allData = {
        "measures": {},
        "alerts": {},
        "status": []
    };

    var updateDisplay = prepareDisplay();

    var namespace = '/stream';
    var socket = io.connect('http://' + document.domain + ':' + location.port + namespace);
    // real time updates
    socket.on('update', function(newData) {

        updateData(allData, newData);
        updateDisplay(allData);
    });
    // initial data
    $.get("/data", function(data) {

        updateData(allData, data);
        updateDisplay(allData);
    });

});

var prepareDisplay = function() {

    var domStatuses = d3.select("#statuses");

    var domAlerts = d3.select("#alerts");

    var margin = {top: 20, right: 20, bottom: 30, left: 50};
    var width = 960 - margin.left - margin.right;
    var height = 500 - margin.top - margin.bottom;

    var parseDate = d3.time.format("%d-%b-%y").parse;

    var x = d3.time.scale()
        .range([0, width]);

    var y = d3.scale.linear()
        .range([height, 0]);

    var xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom");

    var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left");

    var measureLine = d3.svg.line()
        .x(function(d) { return x(d.date); })
        .y(function(d) { return y(d.value); });

    var svg = d3.select("#loadgraph").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
      .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var updateDisplay = function(data) {

        // List of statuses
        var statuses = domStatuses.selectAll("p").data(data.statuses);

        statuses.enter().append("p");
        statuses.exit().remove();
        statuses
            .attr("class", function(d) { return d.alerting ? "bg-danger" : "bg-success"; })
            .text(function(d) { return d.metric + ": " + d.average; });

        svg.selectAll("*").remove();

        // Alert log
        var alerts = domAlerts.selectAll("p")
            .data(Object.values(data.alerts).reverse());

        alerts.enter().append("p");
        alerts.exit().remove();
        alerts
            .attr("class", function(d) {
                return d.event === EVENT_START_ALERT ? "bg-danger" : "bg-success";
            })
            .text(function(d) {
                var date = new Date(d.time * 1000);
                var type = d.event === EVENT_START_ALERT ? "Start" : "Stop";
                return date + " - " + type + " Alert for " + d.metric + ": " + d.value;
            });

        // Graph
        svg.selectAll("*").remove();

        var measuresData = getMeasuresForGraph(data);
        var alertsData = getAlertsForGraph(data);

        // min vertical range for the graph
        var minMaxValue = 10;
        x.domain(d3.extent(measuresData, function(d) { return d.date; }));
        y.domain([
            0,
            Math.max(
                minMaxValue,
                d3.max(measuresData, function(d) { return d.value; })
            )
        ]);

        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis);

        svg.append("g")
            .attr("class", "y axis")
            .call(yAxis)
          .append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", 6)
            .attr("dy", ".71em")
            .style("text-anchor", "end")
            .text("Load");

        svg.append("path")
          .datum(measuresData)
            .attr("class", "measureLine")
            .attr("d", measureLine);

        svg.selectAll(".alert")
            .data(alertsData)
          .enter().append("rect")
            .attr("class", function(d) {
                return "alert " + (d.event === EVENT_START_ALERT ? "redAlert" : "greenAlert");
            })
            .attr("x", function(d) { return x(d.date); })
            .attr("y", function(d) { return y(d.value); })
            .attr("height", function(d) { return height - y(d.value); })
            .attr("width", 4);

        return;
    };
    return updateDisplay;
};

var getMeasuresForGraph = function(data) {

    var measuresData = [];
    for (var k in data.measures) {
        if (data.measures[k].metric === "load") {
            var measure = {
                "date": new Date(data.measures[k].time * 1000),
                "value": data.measures[k].value,
            };
            measuresData.push(measure);
        }
    }
    return measuresData;
};

var getAlertsForGraph = function(data) {

    var alertsData = [];
    for (var k in data.alerts) {
        if (data.alerts[k].metric === "load") {
            var alert = {
                "date": new Date(data.alerts[k].time * 1000),
                "value": data.alerts[k].value,
                "event": data.alerts[k].event,
            };
            alertsData.push(alert);
        }
    }
    return alertsData;
};
