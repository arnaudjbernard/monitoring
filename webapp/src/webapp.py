"""
Main web application.
"""
from flask import Flask, render_template, request, jsonify
from flask.ext.socketio import SocketIO

import logging
import time

import alerting
import database
import outputs
import realtime


app = Flask(__name__)
app.debug = True
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)

realtime.socketio = socketio

DEFAULT_DATA_TO_DISPLAY = 10 * 60  # seconds


@app.route("/")
def main():
    return render_template('index.html')


@app.route("/data", methods=["GET"])
def get_data():

    from_time = time.time() - DEFAULT_DATA_TO_DISPLAY

    measures = database.query_measures(from_time)
    alerts = database.query_alerts(from_time)

    return jsonify(outputs.response(measures, alerts, alerting.get_statuses()))


@app.route("/data", methods=["POST"])
def post_data():

    request_json = request.get_json()
    measure_time = request_json["measure_time"]

    metrics = request_json["metrics"]

    # TODO make the services support batching
    for metric_name, metric_value in metrics.iteritems():
        alerting.add_measure(measure_time, metric_name, metric_value)
        database.save_measure(measure_time, metric_name, metric_value)
        realtime.dispatch_measure(measure_time, metric_name, metric_value)

    return jsonify({"status": "OK"})


@socketio.on('connect', namespace='/stream')
def stream_connect():
    logging.info('Client disconnected')


@socketio.on('disconnect', namespace='/stream')
def stream_disconnect():
    logging.info('Client disconnected')


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)

    socketio.run(app)
