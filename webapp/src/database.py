"""
Simple database storing alerts and measure points.
"""
import os
import sqlite3

import outputs

DATABASE_FILE_PATH = os.path.join(os.path.dirname(__file__), 'database.sqlite3')


def _initDB():
    """
    Initialize a sqlite3 databse with a alerts and a measures table.
    """
    conn = sqlite3.connect(DATABASE_FILE_PATH)
    c = conn.cursor()
    c.execute("CREATE TABLE IF NOT EXISTS measures (time REAL, metric TEXT, value REAL)")
    c.execute("CREATE TABLE IF NOT EXISTS alerts (time REAL, metric TEXT, value REAL, event TEXT)")
    conn.commit()
    conn.close()

# init at import time for convenience
_initDB()


def _execute(cmd, params, fetch=False):
    """
    Execute a command with params on the db.
    @param params: the query sql variables
    @param fetch: if true fetches the result of the query and returns init
    """
    res = None

    conn = sqlite3.connect(DATABASE_FILE_PATH)
    c = conn.cursor()
    c.execute(cmd, params)
    if fetch:
        res = c.fetchall()
    conn.commit()
    conn.close()

    return res


def save_measure(measure_time, metric, value):

    _execute("INSERT INTO measures VALUES(?, ?, ?)",
             (measure_time, metric, value))


def save_start_alert(start_time, metric, value):

    _execute("INSERT INTO alerts VALUES(?, ?, ?, ?)",
             (start_time, metric, value, outputs.EVENT_START_ALERT))


def save_stop_alert(stop_time, metric, value):

    _execute("INSERT INTO alerts VALUES(?, ?, ?, ?)",
             (stop_time, metric, value, outputs.EVENT_STOP_ALERT))


def query_measures(from_time):

    res = []
    measures_col = _execute("SELECT * FROM measures WHERE time > ?", (from_time,), fetch=True)
    for col in measures_col:
        res.append(outputs.measure(col[0], col[1], col[2]))
    return res


def query_alerts(from_time):

    res = []
    alerts_col = _execute("SELECT * FROM alerts WHERE time > ?", (from_time,), fetch=True)
    for col in alerts_col:
        res.append(outputs.alert(col[0], col[1], col[2], col[3]))
    return res
