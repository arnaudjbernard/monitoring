"""
Define the output format of the API.
"""

EVENT_START_ALERT = "start_alert"
EVENT_STOP_ALERT = "stop_alert"


def measure(measure_time, metric_name, metric_value):
    return {
        "time": measure_time,
        "metric": metric_name,
        "value": metric_value,
    }


def alert(measure_time, metric_name, metric_value, event):
    return {
        "time": measure_time,
        "metric": metric_name,
        "value": metric_value,
        "event": event,
    }


def status(metric_name, alert_ongoing, avg_load):
    return {
        "metric": metric_name,
        "alerting": alert_ongoing,
        "average": avg_load,
    }


def response(measures, alerts, statuses):
    return {
        "measures": measures,
        "alerts": alerts,
        "statuses": statuses,
    }
