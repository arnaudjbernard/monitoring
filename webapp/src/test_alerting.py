import mock
import time
import unittest

import alerting


class AlertingUnitTestCase(unittest.TestCase):

    def setUp(self):
        super(AlertingUnitTestCase, self).setUp()

        self.start_time = time.time()

        self.start_alert_mock = mock.patch("alerting._start_alert")
        self.stop_alert_mock = mock.patch("alerting._stop_alert")
        self.start_alert_mock.start()
        self.stop_alert_mock.start()

    def tearDown(self):
        self.start_alert_mock.stop()
        self.stop_alert_mock.stop()

        super(AlertingUnitTestCase, self).tearDown()

    def test_update_load_status_under_avg_value(self):

        load_cache = alerting.MetricCache()
        load_cache.data_points = [
            alerting.DataPoint(self.start_time, 0),
            alerting.DataPoint(self.start_time + alerting.MIN_ALERTING_DATA_DURATION + 1, 0),
        ]
        load_cache.avg_value = alerting.ALERTING_THRESHOLD - 1

        # test does nothing if alert not ongoing
        load_cache.alert_ongoing = False
        alerting._update_load_status(load_cache)
        self.assertFalse(alerting._start_alert.called)
        self.assertFalse(alerting._stop_alert.called)

        # test stop if alert ongoing
        load_cache.alert_ongoing = True
        alerting._update_load_status(load_cache)
        self.assertFalse(alerting._start_alert.called)
        self.assertTrue(alerting._stop_alert.called)

    def test_update_load_status_above_avg_value(self):

        load_cache = alerting.MetricCache()
        load_cache.data_points = [
            alerting.DataPoint(self.start_time, 0),
            alerting.DataPoint(self.start_time + alerting.MIN_ALERTING_DATA_DURATION + 1, 0),
        ]
        load_cache.avg_value = alerting.ALERTING_THRESHOLD + 1

        # test does nothing if alert is already ongoing
        load_cache.alert_ongoing = True
        alerting._update_load_status(load_cache)
        self.assertFalse(alerting._start_alert.called)
        self.assertFalse(alerting._stop_alert.called)

        # test start if alert not ongoing
        load_cache.alert_ongoing = False
        alerting._update_load_status(load_cache)
        self.assertTrue(alerting._start_alert.called)
        self.assertFalse(alerting._stop_alert.called)

    def test_update_load_status_under_min_duration(self):

        # test under MIN_ALERTING_DATA_DURATION does not trigger alerts
        load_cache = alerting.MetricCache()
        load_cache.data_points = [
            alerting.DataPoint(self.start_time, 0),
            alerting.DataPoint(self.start_time + alerting.MIN_ALERTING_DATA_DURATION - 1, 0),
        ]
        load_cache.avg_value = alerting.ALERTING_THRESHOLD + 1
        alerting._update_load_status(load_cache)

        self.assertFalse(alerting._start_alert.called)

    def test_update_load_status_above_min_duration(self):
        # test above MIN_ALERTING_DATA_DURATION does trigger
        load_cache = alerting.MetricCache()
        load_cache.data_points = [
            alerting.DataPoint(self.start_time, 0),
            alerting.DataPoint(self.start_time + alerting.MIN_ALERTING_DATA_DURATION + 1, 0),
        ]
        load_cache.avg_value = alerting.ALERTING_THRESHOLD + 1

        alerting._update_load_status(load_cache)

        self.assertTrue(alerting._start_alert.called)


class AlertingIntegrationTestCase(unittest.TestCase):

    def setUp(self):
        super(AlertingIntegrationTestCase, self).setUp()

        self.realtime_mock = mock.patch("alerting.realtime")
        self.database_mock = mock.patch("alerting.database")

        self.realtime_mock.start()
        self.database_mock.start()

    def tearDown(self):
        self.realtime_mock.stop()
        self.database_mock.stop()

        super(AlertingIntegrationTestCase, self).tearDown()

    def test_alerting_cycle(self):
        """
        Integration test. Tests if a typical load profile triggers the expected alerts.
        """

        measure_time = time.time()

        # sends 3 minutes of non alerting
        for _ in range(0, 60 * 3, 10):
            alerting.add_measure(measure_time, "load", 0.0)
            measure_time += 10
            self.assertEqual(alerting.cache["load"].alert_ongoing, False)
            self.assertEqual(alerting.cache["load"].avg_value, 0)

        # sends 2 minutes of barely alerting
        for _ in range(0, 60 * 2, 10):
            alerting.add_measure(measure_time, "load", 0.0001 + alerting.ALERTING_THRESHOLD)
            measure_time += 10
            self.assertEqual(alerting.cache["load"].alert_ongoing, False)

        # next point should start alert
        alerting.add_measure(measure_time, "load", 0.0001 + alerting.ALERTING_THRESHOLD)
        measure_time += 10
        self.assertEqual(alerting.cache["load"].alert_ongoing, True)
        self.assertAlmostEqual(alerting.cache["load"].avg_value,
                               0.0001 + alerting.ALERTING_THRESHOLD)

        # make sure the other services hooks are called
        self.assertTrue(alerting.realtime.dispatch_start_alert.called)
        self.assertTrue(alerting.database.save_start_alert.called)
        self.assertFalse(alerting.realtime.dispatch_stop_alert.called)
        self.assertFalse(alerting.database.save_stop_alert.called)
        alerting.realtime.reset_mock()
        alerting.database.reset_mock()

        # sends 2 minutes of seriously alerting
        for _ in range(0, 60 * 2, 10):
            alerting.add_measure(measure_time, "load", 1000 * alerting.ALERTING_THRESHOLD)
            measure_time += 10
            self.assertEqual(alerting.cache["load"].alert_ongoing, True)

        # sends 2 minutes of non alerting
        for _ in range(0, 60 * 2, 10):
            alerting.add_measure(measure_time, "load", 0.0)
            measure_time += 10
            self.assertEqual(alerting.cache["load"].alert_ongoing, True)

        # next point should stop alert
        alerting.add_measure(measure_time, "load", 0.0)
        measure_time += 10
        self.assertEqual(alerting.cache["load"].alert_ongoing, False)
        self.assertAlmostEqual(alerting.cache["load"].avg_value, 0)

        # make sure the other services hooks are called
        self.assertFalse(alerting.realtime.dispatch_start_alert.called)
        self.assertFalse(alerting.database.save_start_alert.called)
        self.assertTrue(alerting.realtime.dispatch_stop_alert.called)
        self.assertTrue(alerting.database.save_stop_alert.called)
        alerting.realtime.reset_mock()
        alerting.database.reset_mock()


if __name__ == "__main__":
    unittest.main(verbosity=2)
