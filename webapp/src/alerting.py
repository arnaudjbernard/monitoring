"""
Alerting service monitoring recent measure and trigering alerts.
"""
import logging
import time

import database
import outputs
import realtime

RUNNING_AVG_DURATION = 60 * 2  # seconds
# minimum amount of time between the point of the running average to alert
MIN_ALERTING_DATA_DURATION = 60 * 1  # seconds
ALERTING_THRESHOLD = 1

# in memory data, emulates a memcache or Redis
# map of metric_name -> MetricCache
cache = {}


class DataPoint(object):
    def __init__(self, measure_time, metric_value):
        self.measure_time = measure_time
        self.metric_value = metric_value


class MetricCache(object):
    def __init__(self):
        self.data_points = []
        self.alert_ongoing = False
        self.avg_value = 0


def add_measure(measure_time, metric_name, metric_value):
    """
    Add a measure point to monitor. Will update cache and trigger alerts if needed.
    """

    if metric_name not in cache:
        cache[metric_name] = MetricCache()

    metric_cache = cache[metric_name]

    # save the new data point
    metric_cache.data_points.append(DataPoint(measure_time, metric_value))

    # filter older ones
    data_points = metric_cache.data_points
    latest_time = data_points[-1].measure_time
    data_points = [data_point for data_point in data_points
                   if data_point.measure_time + RUNNING_AVG_DURATION >= latest_time]
    metric_cache.data_points = data_points

    if data_points:
        # compute the average
        metric_values = [data_point.metric_value for data_point in data_points]
        # ignore missing points, no interpolation
        avg_value = sum(metric_values) / len(metric_values)
        metric_cache.avg_value = avg_value
    else:
        metric_cache.avg_value = 0

    # custom rule for load alerting
    if metric_name == "load":
        _update_load_status(metric_cache)


def get_statuses():
    """
    Return the current status of the metrics being monitored.
    """
    statuses = []
    for metric_name, metric_status in cache.iteritems():
        status = outputs.status(metric_name,
                                metric_status.alert_ongoing,
                                metric_status.avg_value)
        statuses.append(status)
    return statuses


def _update_load_status(load_cache):
    """
    Implement custom alerting for load. Triggers an alert if enough
    data has been collected and the laod is above the threshold.
    """

    load_alert_ongoing = load_cache.avg_value > ALERTING_THRESHOLD

    # don't start alerting if we have less than a minute of data
    if not load_cache.alert_ongoing and load_cache.data_points:
        first_measure_time = load_cache.data_points[0].measure_time
        last_measure_time = load_cache.data_points[-1].measure_time
        diff_time = last_measure_time - first_measure_time
        if diff_time < MIN_ALERTING_DATA_DURATION:
            return

    if load_alert_ongoing != load_cache.alert_ongoing:
        load_cache.alert_ongoing = load_alert_ongoing
        if load_alert_ongoing:
            _start_alert("load rule", "load", load_cache.avg_value)
        else:
            _stop_alert("load rule", "load", load_cache.avg_value)


def _start_alert(rule, metric_name, avg_value):

    start_time = time.time()

    logging.info("Starting Alert: %s - load: %s, start_time: %s", rule, avg_value, start_time)

    realtime.dispatch_start_alert(start_time, metric_name, avg_value)
    database.save_start_alert(start_time, metric_name, avg_value)


def _stop_alert(rule, metric_name, avg_value):

    stop_time = time.time()

    logging.info("Stoping Alert: %s - load: %s, stop_time: %s", rule, avg_value, stop_time)

    realtime.dispatch_stop_alert(stop_time, metric_name, avg_value)
    database.save_stop_alert(stop_time, metric_name, avg_value)
