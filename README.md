# Intro

This is a simple webapp to monitor the load and free memory on a machine.

`collector.py` collects data from the system.
`webapp.py` stores data and monitors it. It offers a web client to look at
the status of the metrics and the alerting.

Example screenshot:
![screenshot](screenshot.png)

# Instructions

### Setup
```
virtualenv env
source env/bin/activate
pip install -r requirements.txt
deactivate
```

### Start webapp
```
source env/bin/activate
python webapp/src/webapp.py
```

### Start collector
In a separate command line to run in parallel with the webapp
```
source env/bin/activate
python collector/src/collector.py
```

### Web page
Go to http://127.0.0.1:5000/

### Run the tests
```
source env/bin/activate
pushd webapp/src
python -m unittest test_alerting
popd
```


# Improvement Suggestions

- collector
    - make the collector a daemon
    - should restart itself automatically
    - retry on error, potentially cache unsent data points
- webapp
    - add intermediary model for the metrics to share accross services
    - use a nosql database to store data points since the query needed is minimal
    - use file to store historical data points (run a job to empty the db)
    - use memcache or Redis rather than instance memory for the alerting cache
    - use a queue system to dispatch events to the services
    - warmup the alerting service at startup with the data from the database
    - input specification and validation
- client
    - improve overall UI
        - support mouse over tooltip on grah
        - have nicer css
    - support display several metrics


# Specifications

Create a simple web application that monitors load average on your machine:

- Collect the machine load (using "uptime" for example)
- Display in the application the key statistics as well as a history of load over the past 10 minutes in 10s intervals. We'd suggest a graphical representation using D3.js, but feel free to use another tool or representation if you prefer. Make it easy for the end-user to picture the situation!
- Make sure a user can keep the web page open and monitor the load on their machine.
- Whenever the load for the past 2 minutes exceeds 1 on average, add a message saying that "High load generated an alert - load = {value}, triggered at {time}"
- Whenever the load average drops again below 1 on average for the past 2 minutes, Add another message explaining when the alert recovered.
- Make sure all messages showing when alerting thresholds are crossed remain visible on the page for historical reasons.
- Write a test for the alerting logic
- Explain how you'd improve on this application design

# Architecture Ideas

- collector.py
    - run at all time
    - every 10s (time.sleep) collect data
    - push to webapp

- webapp.py (should be split between collection and view for separation of concern)
    - run flask app
    - one endpoint to collect data (should enqueue tasks)
        - feed a db
        - feed real time socket
        - feed real time monitoring for alerting
    - one endpoint to view data
        - render html page
            - html page opens a socket to update data
            - html page fetches data (merge with socket)
            - may need to fetch additionnal data
            - display graph and alerts using D3.js
            - display key statistics (alerting status, metric 2 average)
    - one endpoint to fetch data (last 10 min)
        - data contains list of points
        - data contains list of alerts (open, close)
    - one socket endpoint to stream real time data
        - sends messages containing any new points or alerts
    - alerting service
        - keeps a running average over 2 min
        - keeps state of alerting
        - when switching alerting state notify socket + db
    - db
        - stores history of points + alerts
        - limited to 10 min -> memory only? or could be two simple SQL tables
    - real time service
        - sends the data to the html page

- testing
    - collect.py
        - mock sys calls, time sleep and http connection
        - make sure all calls are made and correct data is sent

    - webapp.py
        - integration send data, retrive same data
        - mock alerting function, feed real time data and assert alerts

- setup
    - create virtualenv
    - pip install all the things
    - have a doc on how to start the webapp and collector

