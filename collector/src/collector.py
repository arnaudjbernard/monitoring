"""
Overly simplified deamon to collect and send data about host load.
"""
import argparse
import logging
import time
import re
import requests
import subprocess

MEASURE_INTERVAL = 10  # seconds
uptime_re = re.compile(r".*load averages: (\d+\.\d+).*")


def measure_loop(url):
    """
    Loop forever measuring and sending data.

    @param url: the url to send data to
    """
    measure_time = time.time()

    while True:
        # reset measure time if we drifted too far away (computer sleep)
        if abs(time.time() - measure_time) > MEASURE_INTERVAL:
            measure_time = time.time()

        measure_and_send_load(measure_time, url)
        measure_time += MEASURE_INTERVAL
        time.sleep(max(0, measure_time - time.time()))


def measure_and_send_load(measure_time, url):
    """
    Measure and send load data.

    @param measure_time: the target time of the measure
    @param url: the url to send data to
    """
    uptime_str = subprocess.check_output("uptime")
    load_match = uptime_re.match(uptime_str)

    if not load_match:
        logging.error("Could not find load information in %s", uptime_str)
        return

    # get free memory information if possible
    free_memory_pages = None
    try:
        vm_stat_cmd = "vm_stat | grep free | awk '{ print $3 }' | sed 's/\.//'"
        free_memory_pages = int(subprocess.check_output(vm_stat_cmd, shell=True))
    except Exception as e:
        logging.info("Could not get free_memory_pages information: %s", e)

    try:
        load = float(load_match.group(1))
    except Exception:
        logging.error("Could not find parse information in %s", uptime_str)
        return

    data = {
        "measure_time": measure_time,
        "metrics": {
            "load": load,
        },
    }

    if free_memory_pages:
        data["metrics"]["free_memory_pages"] = free_memory_pages

    try:
        r = requests.post(url, json=data)
        r.raise_for_status()
        logging.info("sent data %s", data)
    except requests.exceptions.RequestException:
        # TODO retry on failure
        logging.error("Could not send data %s", data)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)

    parser = argparse.ArgumentParser(description='Deploy the backend.')
    parser.add_argument('url', nargs='?', default='http://127.0.0.1:5000/data',
                        help="url of the webapp to post events to")
    args = parser.parse_args()
    measure_loop(args.url)
